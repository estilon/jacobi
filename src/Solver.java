import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * Created by estilon on 20/06/16.
 */
public class Solver extends Thread {
    static int i_c = -1;
    private final float[][] matrixA;
    private final float[] matrixB;
    private final double[] matrixC;
    private final double[] temp;
    private final Semaphore semaphore;
    private final CyclicBarrier barrier;
    private int c = 0;

    public Solver(float[][] matrixA, float[] matrixB, double[] matrixC, double[] temp, Semaphore semaphore, CyclicBarrier barrier ){
        this.matrixA = matrixA;
        this.matrixB = matrixB;
        this.matrixC = matrixC;
        this.semaphore = semaphore;
        this.temp = temp;
        this.barrier = barrier;
    }

    public void run(){
        int i = 0;
        while(c++ < 10000){
            while(i_c < matrixA.length){
                try {
                    semaphore.acquire();

                    i_c++;
                    i = i_c;

                    if(i >= matrixA.length){
                        semaphore.release();
                        break;
                    }

                    semaphore.release();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                double a = 0;
                for(int j = 0; j < matrixA.length; j++){
                    a += i == j ? (matrixB[i])/matrixA[i][i] : (matrixA[i][j]/(-matrixA[i][i])) * matrixC[j];
                }
                temp[i] =  a;

            }

            try {
                //System.out.println("Numero esperando " + barrier.getNumberWaiting());
                barrier.await();
                if(verificar()) {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean verificar(){
        boolean retorno = true;
        for(int i = 0; i < matrixA.length; i++){
            float soma = 0;
            for(int j = 0; j < matrixA.length; j++){
                soma += matrixA[i][j] * matrixC[j];
            }
            //System.out.println("Soma "+soma + ", coisa lá: "+matrixB[i] + " diferenca = " + (matrixB[i] - soma)  );
            if(Math.abs(matrixB[i] - soma) < 0.1){
                //System.out.println("Corrert!");
                retorno = true;
            }else{
                //System.out.println("No correct");
                return false;
            }
        }
        return retorno;
    }

    public int iteracoes(){
        return --c;
    }
}
