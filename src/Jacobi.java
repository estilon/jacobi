import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

import static java.lang.Math.ceil;

public class Jacobi {
	
	private float[][] matrixA;
	private float[] matrixB;
	private double[] matrixC;
	private double[] matrixCTemp;
	private int c;
	
	public Jacobi(float[][] matrixA, float[] matrixB){
		this.matrixA = matrixA;
		this.matrixB = matrixB;
		this.matrixC = new double[matrixB.length];
		this.matrixCTemp = new double[matrixB.length];
	}
	
	public double[] raiz(){
		iniciarC();
		iterar();
		return matrixC.clone();
	}
	
	private void iniciarC(){
		for(int i = 0; i < matrixC.length; i++){
			matrixC[i] = 0;
		}
	}
	
	/*private void iterar(){
		
		float[] temp = new float[matrixC.length];
		int c = 0;
		while(c++ < 10000){
			for(int i = 0; i < matrixA.length; i++){
				float a = 0;
				for(int j = 0; j < matrixA.length; j++){
					a += i == j ? (matrixB[i])/matrixA[i][i] : (matrixA[i][j]/(-matrixA[i][i])) * matrixC[j];
				}
				temp[i] =  a;
			}

			for(int i = 0; i < matrixA.length; i++){

				matrixC[i] = temp[i];

			}

		}
		
		verificar();
	}*/

	private void iterar(){
		int nucleos = Runtime.getRuntime().availableProcessors();

		CyclicBarrier barrier = new CyclicBarrier(nucleos, new Runnable() {
			@Override
			public void run() {
				for(int i = 0; i < matrixA.length; i++){

					matrixC[i] = matrixCTemp[i];

				}
				Solver.i_c = -1;
			}
		});

		Semaphore semaphore = new Semaphore(nucleos);

		Solver[] solvers = new Solver[nucleos];
		for(int i = 0; i < nucleos; i++){
			solvers[i] = new Solver(matrixA, matrixB, matrixC, matrixCTemp, semaphore, barrier);
			solvers[i].start();
		}
		for(int i = 0; i < nucleos; i++){
			try {
				solvers[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		c = solvers[0].iteracoes();

	}

	public boolean verificarCongruencia(){
		boolean converge = true;
		for(int i = 0; i < matrixA.length; i++){
			int soma = 0;

			for(int j = 0; j < matrixA.length; j++){
				soma += i == j ? 0 : matrixA[i][j] ;
			}

			if(((0.1) * soma)/matrixA[i][i] >= 1){
				converge = false;
			}
		}

		if(converge) return true;

		for(int i = 0; i < matrixA.length; i++){
			int soma = 0;
			for(int j = 0; j < matrixA.length; j++){
				soma += i == j ? 0 : matrixA[j][i];
			}

			if(((0.1) * soma)/matrixA[i][i] >= 1){
				converge = false;
			}
		}
		return  converge;
	}

	private boolean verificar(){
		boolean retorno = true;
		for(int i = 0; i < matrixA.length; i++){
			float soma = 0;
			for(int j = 0; j < matrixA.length; j++){
				soma += matrixA[i][j] * matrixC[j];
			}
			//System.out.println("Soma "+soma + ", coisa lá: "+matrixB[i] + " diferenca = " + (matrixB[i] - soma)  );
			if(Math.abs(matrixB[i] - soma) < 0.1){
				//System.out.println("Corrert!");
				retorno = true;
			}else{
				//System.out.println("No correct");
				return false;
			}
		}
		return retorno;
	}

	public int iteracoes(){
		return c;
	}

	public double diferenca(int i){
		double soma = 0.0f;
		for(int j = 0; j < matrixA.length; j++){
			soma += matrixA[i][j] * matrixC[j];
		}

		return matrixB[i] - soma;
	}

}
