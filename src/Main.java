import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	private static int n;
	private static int numberOfThreadsByProcessor;
	private static int verificationLine;
	private static int maxNumberOfInteractions;

	public static void main(String args[]){

		for(int i = 1; i < 6; i++) {
			long time;
			time = System.currentTimeMillis();
			lerEntradas("/home/estilon/IdeaProjects/jacobi/matriz3.dat");
			time = System.currentTimeMillis() - time;
			System.out.println("\nTempo de execução " + time / (double) 1000);
		}

	}

	private static void lerEntradas(String caminho) {
		BufferedReader br = null;

		try{

			br = new BufferedReader(new FileReader(caminho));

			String line = null;

			n = Integer.parseInt(br.readLine());

			numberOfThreadsByProcessor = Integer.parseInt(br.readLine());
			verificationLine = Integer.parseInt(br.readLine());
			maxNumberOfInteractions = Integer.parseInt(br.readLine());

			Scanner scanner = new Scanner(br);

			float[][] matrixA = new float[n][n];
			float[] matrixB = new float[n];

			for(int i = 0; i < n; i++){
				for(int j = 0; j < n; j++){
					matrixA[i][j] = scanner.nextFloat();
				}
			}

			for(int i = 0; i < n; i++){
				matrixB[i] = scanner.nextFloat();
			}

			scanner.close();

			Jacobi jacobi = new Jacobi(matrixA, matrixB);

			if(jacobi.verificarCongruencia()){

				double[] matrixC = jacobi.raiz();
				for(int i = 0; i < n; i++){

					System.out.print(matrixC[i] + " ");
				}

				System.out.println("\nNumero de iteracoes " + jacobi.iteracoes());

				System.out.println("Diferença " + jacobi.diferenca(verificationLine));

			}else {
				System.out.println("A matriz não converge");
			}


		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
